"""
Neat is a simple way to normalize indentation of HTML files based on really 
primitive tag parsing rules. No modifications to the HTML files will be 
made except change the indentation of each line.

"""

from __future__ import absolute_import

__author__ = "Jordan Sherer <jordan@widefido.com>"
__version__ = "0.0.1"

import fnmatch
import logging
import os
import os.path
import re

logging.root.setLevel(logging.NOTSET)

log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())

indent_re = re.compile("^(\s+)")
tag_re = re.compile("""<(?:"[^"]*"['"]*|'[^']*'['"]*|[^'">])+>""")
name_re = re.compile("""<(/?[^>/\s]*)""")

def tag_name(tag):
    """
    For a given HTML tag, try to pull out the tag name

    >>> tag_name("<a href=''>")
    'a'
    >>> tag_name("<coption:b/>")
    'coption:b'

    """
    match = name_re.match(tag)
    if match:
        return match.group(1)
    return None 


def transform(line, indent, indent_char="\t"):
    """
    Transform a line of text to be indented at a specific level using the 
    indent_char as the line prefix.
    
    NOTE: all whitespace is stripped for the end of the line to aid in spacing

    """
    normalized_line = indent_re.sub("", line).rstrip()
    if not normalized_line:
        return None, indent


    indent = 0 if indent < 0 else indent
    next_indent = indent

    # find the tags in the current line that will indent the next line
    # i.e., all tags that are either self closing or that we want to ignore
    tags = [tag for tag in tag_re.findall(normalized_line) if
            not tag.endswith("/>")       and
            not tag.startswith("<html")  and
            not tag.startswith("<meta")  and
            not tag.startswith("<link")  and
            not tag.startswith("<br")    and
            not tag.startswith("<hr")    and
            not tag.startswith("<img")   and
            not tag.startswith("<input") and
            not tag.startswith("<col")   and
            not tag.startswith("<base")  and
            not tag.startswith("<area")  and
            not tag.startswith("<!")]

    # detect if if tags are opened and closed on the same line
    open_stack = []
    close_stack = []
    for tag in tags:
        name = tag_name(tag)
        if not name:
            continue
        
        if name[0] != "/":
            open_stack.append(name)
        else:
            close_stack.append(name)
            while open_stack:
                val = open_stack.pop()
                if val == name.lstrip("/"):
                    close_stack.pop()
                    break

    # if we have open tags, indent the next line to match
    if open_stack:
        next_indent = indent + len(open_stack)
    
    # if we have closing tags, dedent the current line to match
    if close_stack:
        indent -= len(close_stack)
        next_indent = indent

    # one off case for head if our markup couldn't detect the opening html tag
    # a common case for this is conditional IE classes on the HTML tag.
    if open_stack and open_stack[0] == "head" and indent == 0:
        indent += 1
        next_indent += 1

    # make sure we never have a negative indent, because that would be silly
    indent = 0 if indent < 0 else indent

    indented_line = "{0}{1}".format(indent_char * indent, normalized_line)

    return indented_line, next_indent


def process(f, indent_char="\t"):
    """
    Process a file, string, or other string iterable and return the results.

    """
    output = []
    indent = 0
    for line in f:
        line, indent = transform(line, indent, indent_char=indent_char) 
        if line:
            output.append(line)
    return "\n".join(output)


def clean(input_file, output_file, indent_char="\t"):
    """
    Clean an input file and write the contents to the output file.

    """
    cleaned = process(input_file)
    with output_file as f:
        f.write(cleaned)

def clean_all(directory, match="*.html", exclude=tuple(), indent="\t", dry_run=False, overwrite=False):
    """ 
    Clean all matching files in a directory.

    """
    prev_path = None
    skip_paths = []
    for path, dirs, files in os.walk(os.path.abspath(directory)):
        if skip_paths and any(path.startswith(skip_path) for skip_path in skip_paths):
            continue

        head, tail = os.path.split(path)
        if exclude and (
                (tail in exclude or any(fnmatch.fnmatch(tail, e) for e in exclude)) or
                (path in exclude or any(fnmatch.fnmatch(path, e) for e in exclude))):
            log.info("Skipping path: {0}".format(path))
            skip_paths.append(path)
            continue

        if prev_path != path:
            log.info("Walking path: {0}".format(path))
            prev_path = path

        for filename in fnmatch.filter(files, match):
            if ".neat" in filename:
                continue

            if exclude and (filename in exclude or any(fnmatch.fnmatch(filename, e) for e in exclude)):
                log.debug("  Skipping {0}...".format(filename))
                continue

            root, ext = os.path.splitext(filename)
            new_filename = "{0}.neat{1}".format(root, ext)

            log.debug("  Cleaning {0} -> {1}...".format(filename, new_filename))

            if dry_run:
                log.debug("  - skipping (dry-run)")
                continue

            with open(os.path.join(path, filename), "r") as f:
                input_file = f.read().split("\n")
            
            with open(os.path.join(path, new_filename), "w") as output_file:
                clean(input_file, output_file)

            if overwrite:
                os.rename(os.path.join(path, new_filename), os.path.join(path, filename))


def clean_cli():
    import argparse
    import sys
    parser = argparse.ArgumentParser(description="Clean a single HTML file's indentation")
    parser.add_argument("input", nargs="?", default=sys.stdin, type=argparse.FileType("r"), help="Input file (default:stdin)")
    parser.add_argument("output", nargs="?", default=sys.stdout, type=argparse.FileType('w'), help="Output file (default:stdout)")
    parser.add_argument("-i", "--indent", default="\t", help="Character to use for indent (default: \\t)")

    args = parser.parse_args()

    clean(args.input, args.output, indent_char=args.indent)

def clean_all_cli():
    import argparse
    import sys

    class ExtendAction(argparse.Action):
        def __call__(self, parser, namespace, values, option_string=None):
            v = getattr(namespace, self.dest)
            if v is None:
                v = []
                setattr(namespace, self.dest, v)
            v.extend(values)

    parser = argparse.ArgumentParser(description="Clean HTML files' indentation")
    parser.add_argument("directory", nargs="?", default=".", help="Directory with files")
    parser.add_argument("-m", "--match", default="*.html", help="Pattern to match files (default: *.html)")
    parser.add_argument("-i", "--indent", default="\t", help="Character to use for indent (default: \\t)")
    parser.add_argument("--dry-run", default=False, action="store_true", help="Don't perform the cleaning")
    parser.add_argument("--exclude", nargs="*", help="Exclude matching files", action=ExtendAction)
    parser.add_argument("--overwrite", default=False, action="store_true", help="Overwrite the original file")
    parser.add_argument("-v", dest="verbose", action="store_true", help="be more verbose")
    parser.add_argument("-q", dest="quiet", action="store_true", help="be quiet (less verbose)")

    args = parser.parse_args()

    if args.quiet:
        log.setLevel(logging.WARNING)
    elif args.verbose:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)

    clean_all(args.directory, match=args.match, indent=args.indent, exclude=args.exclude, dry_run=args.dry_run, overwrite=args.overwrite)


if __name__ == "__main__":
    cli()
