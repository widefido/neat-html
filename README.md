# Neat

A simple script to normalize indentation of HTML using tag nesting rules.

## Usage

    usage: scripts/neat [-h] [-i INDENT] [input] [output]
    
    Clean an HTML file's indentation
    
    positional arguments:
      input                 Input file (default:stdin)
      output                Output file (default:stdout)
    
    optional arguments:
      -h, --help            show this help message and exit
      -i INDENT, --indent INDENT
                            Character to use for indent (default: \t)

# Neat-All

A simple script to normalize indentation of HTML files in a directory using tag nesting rules.

## Usage

    usage: scripts/neat-all [-h] [-m MATCH] [-i INDENT] [--dry-run] [--exclude [EXCLUDE [EXCLUDE ...]]] [directory]

    Clean HTML files' indentation

    positional arguments:
    directory             Directory with files

    optional arguments:
    -h, --help            show this help message and exit
    -m MATCH, --match MATCH
                            Pattern to match files (default: *.html)
    -i INDENT, --indent INDENT
                            Character to use for indent (default: \t)
    --dry-run             Don't perform the cleaning
    --exclude [EXCLUDE [EXCLUDE ...]]
                            Exclude matching files
