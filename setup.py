from distutils.core import setup

from neat import __version__

setup(
    name="neat-html",
    version=__version__,
    description="A simple script to normalize indentation of HTML using tag nesting rules.",
    author="Jordan Sherer",
    author_email="jordan@widefido.com",
    url="http://bitbucket.org/widefido/neat-html",
    keywords=["html", "static", "clean", "indent"],
    py_modules=["neat"],
    scripts=["scripts/neat", "scripts/neat-all"],
    classifiers=[
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.2",
        "Programming Language :: Python :: 3.3",
        "Development Status :: 4 - Beta",
    ],
)
